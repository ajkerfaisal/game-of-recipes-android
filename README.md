# README #

### What is this repository for? ###

* This app is created as skill demonstration for affinitas GmbH	
* Version: 0.0.1

### How do I get set up? ###

* open project and build
		
### Dependencies ###
* retrofit
* butterknife
* picasso

### Who do I talk to? ###

* Mohammad Faisal
* ajkerfaisal@yahoo.com
