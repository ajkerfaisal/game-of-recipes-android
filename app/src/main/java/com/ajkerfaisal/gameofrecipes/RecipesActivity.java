package com.ajkerfaisal.gameofrecipes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import com.ajkerfaisal.gameofrecipes.adapters.RecipesAdapter;
import com.ajkerfaisal.gameofrecipes.models.Recipe;
import com.ajkerfaisal.gameofrecipes.network.RecipeAPI;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecipesActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecipesAdapter recipesAdapter;
    private List<Recipe> recipeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes);
        prepareOnLoad();
        fetchRecipes();
    }

    private void prepareOnLoad(){

        //init recylerview and its adapter
        recyclerView = (RecyclerView) findViewById(R.id.recylerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recipeList = new ArrayList<>();
        recipesAdapter = new RecipesAdapter(recipeList, this);
        recyclerView.setAdapter(recipesAdapter);
    }

    private void fetchRecipes(){

        //fetch recipes
        RecipeAPI.Factory.getInstance().getRecipes().enqueue(new Callback<Recipe>() {
            @Override
            public void onResponse(Call<Recipe> call, Response<Recipe> response) {
                Log.i("Recipes",response.body().getId());
            }

            @Override
            public void onFailure(Call<Recipe> call, Throwable t) {
                Log.e("error","fetch recipes");

                //fetch recipes from local json file
                String json = null;
                try {
                    InputStream is = getResources().openRawResource(R.raw.recipes);
                    int size = is.available();
                    byte[] buffer = new byte[size];
                    is.read(buffer);
                    is.close();
                    json = new String(buffer, "UTF-8");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                //parse
                try {

                    Gson gson = new Gson();
                    List<Recipe> recipesFromJson = new ArrayList<Recipe>(Arrays.asList(gson.fromJson(json, Recipe[].class)));
                    recipeList.addAll(recipesFromJson);
                    recipesAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
