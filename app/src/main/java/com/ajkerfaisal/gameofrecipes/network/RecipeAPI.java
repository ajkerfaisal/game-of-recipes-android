package com.ajkerfaisal.gameofrecipes.network;

import com.ajkerfaisal.gameofrecipes.models.Recipe;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
/**
 * Created by Mohammad.Faisal on 8/14/17.
 */

public interface RecipeAPI {

    String BASE_URL = "http://www.gameofrecipes/api/";

    @GET("recipes/") Call<Recipe> getRecipes();

    class Factory{

        private static RecipeAPI service;

        public static RecipeAPI getInstance(){
            if (service == null) {
                Retrofit retrofit = new Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(BASE_URL)
                        .build();
                service = retrofit.create(RecipeAPI.class);
            }
            return service;
        }
    }




}
